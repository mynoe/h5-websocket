// vue.config.js
var path = require('path')
const autoprefixer = require('autoprefixer');
const pxtorem = require('postcss-pxtorem');
const pxtoviewport = require('postcss-px-to-viewport');
function resolve(dir) {
  return path.join(__dirname, './', dir)
}
module.exports = {
  publicPath: './',
  outputDir: '../www',
  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          autoprefixer(),
        //   pxtorem({
        //     rootValue: 37.5,
        //     propList: ['*'],
        //     // 该项仅在使用 Circle 组件时需要
        //     // 原因参见 https://github.com/youzan/vant/issues/1948
        //     selectorBlackList: ['van-circle__layer']
        //   })
          pxtoviewport({
            viewportWidth: 375,
            // 该项仅在使用 Circle 组件时需要
            // 原因参见 https://github.com/youzan/vant/issues/1948
            selectorBlackList: ['van-circle__layer']
          })
        ]
      }
    }
  },
  configureWebpack: {
    module: {
      rules: [
        {
          test: /\.vue$/,
        //   use: [
        //     {
        //       loader: 'vue-loader',
        //       options: {
        //         prefix: true
        //       }
        //     }
        //   ]
        }
      ]
    }
  },
  devServer: {
    proxy: {
      '/api': {
        target: 'https://assets.iddata.cc',
        changeOrigin: true,
        pathRewrite: {
          '^/api': ''
        }
      }
    },
        hot:true,
        inline: true
    }
}