import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/views/login/Login.vue'
import Bind from '@/views/bind/bind.vue'
import BindDetail from '@/views/bind/bindDetail'
import BindCompany from '@/views/bind/bindCompany'
import Home from '@/views/home/Home'
import HomeIndex from '@/views/home/HomeIndex'

Vue.use(Router)

export default new Router({
//   mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/bind',
      name: 'bind',
      component: Bind
    },
    {
      path: '/bindDetail',
      name: 'bindDetail',
      component: BindDetail
    },
    {
      path:'/bindCompany',
      name: 'bindCompany',
      component: BindCompany
    },
    {
      path: '/home/',
      name: 'about',
    //   route level code-splitting
    //   this generates a separate chunk (about.[hash].js) for this route
    //   which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ '@/views/home/Home.vue'),
      component:Home,
      children: [
        {
          // UserProfile will be rendered inside User's <router-view>
          // when /user/:id/profile is matched
          path: 'index',
          component: HomeIndex
        },
      ]
    }
  ]
})