import Vue from 'vue'
import App from './App.vue'
import Vant from 'vant'
import store from '@/store/'
import router from '@/router/index'
import 'vant/lib/index.css'
import api from './http'
import {Toast} from 'vant'
import { Notify } from 'vant';
import ws from './utils/ws'


import 'amfe-flexible';
Vue.prototype.$ws = ws


Vue.use(Vant);
Vue.use(Notify);
Vue.use(Toast);

Vue.config.productionTip = false
Vue.prototype.$api = api

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
