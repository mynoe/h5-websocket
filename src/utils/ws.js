var websocket;//websocket实例
var lockReconnect = false;//避免重复连接

var wsUrl = " wss://assets.iddata.cc/watch/test01";
let data = {}

function createWebSocket(url) {
    try {
        websocket = new WebSocket(url);
        initEventHandle();
    } catch (e) {
        reconnect(url);
    }     
}

function initEventHandle() {
    websocket.onclose = function () {
        console.log("WebSocket连接已经关闭");
        // reconnect(wsUrl);
    };
    websocket.onerror = function () {
        console.log("WebSocket连接发生错误");
        reconnect(wsUrl);
    };
    websocket.onopen = function () {
        console.log("WebSocket连接已经成功");
        //心跳检测重置
        heartCheck.reset().start();
    };
    websocket.onmessage = function (event) {console.log(event.data)
        setMessageInnerHTML(event.data);
        //如果获取到消息，心跳检测重置
        //拿到任何消息都说明当前连接是正常的
        heartCheck.reset().start();
    }
}

//重新连接webSocket
function reconnect(url) {
    if(lockReconnect) return;
    lockReconnect = true;
    //没连接上会一直重连，设置延迟避免请求过多
    setTimeout(function () {
        createWebSocket(url);
        lockReconnect = false;
    }, 2000);
}

//心跳检测
var heartCheck = {
    timeout: 5000,//60秒
    timeoutObj: null,
    serverTimeoutObj: null,
    reset: function(){
        clearTimeout(this.timeoutObj);
        clearTimeout(this.serverTimeoutObj);
        return this;
    },
    start: function(){
        var self = this;
        this.timeoutObj = setTimeout(function(){
            //这里发送一个心跳，后端收到后，返回一个心跳消息，
            //onmessage拿到返回的心跳就说明连接正常
            websocket.send("HeartBeat");
            self.serverTimeoutObj = setTimeout(function(){//如果超过一定时间还没重置，说明后端主动断开了
                websocket.close();//如果onclose会执行reconnect，我们执行ws.close()就行了.如果直接执行reconnect 会触发onclose导致重连两次
            }, self.timeout)
        }, this.timeout)
    }
}


//将消息显示在网页上
function setMessageInnerHTML(innerHTML) {
    console.log(typeof innerHTML);
    // data = innerHTML
    var obj = eval('(' + innerHTML + ')');
    console.log(typeof obj)
    data = obj
    closeWebSocket()
    //var data = JSON.parse(innerHTML);
    console.log("-------------- websocket -----------------------------------");
    
}

//关闭WebSocket连接
function closeWebSocket() {
    websocket.close();
}

//发送消息
function send(message) {
    websocket.send(message);
}

let ws = {
    start:function(){
        //判断当前浏览器是否支持WebSocket
        if ('WebSocket' in window) {
            //初始化连接webSocket
            console.log('---this--')
        console.log(this)
            createWebSocket(wsUrl);
        }
        else {
            alert('当前浏览器 Not support websocket')
        }
    },
    getData:function(){
        // console.log('---this--')
        // console.log(this)
        // var nameData={}
        return  data
    },
    end:function(){
        websocket=null
    }

}

export default ws