import Vue from 'vue'
import Vuex from 'vuex'
// import resourcePanel from './modules/resourcePanel'
// import resourceEditQueue from './modules/resourceEditQueue'
// import templateManage from './modules/templateManage'
// import sidebar from './modules/sidebar'
// import sideMenus from './modules/sideMenus'
// import viewUse from './modules/viewUse'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    // resourcePanel,
    // resourceEditQueue,
    // templateManage,
    // sidebar,
    // sideMenus,
    // viewUse
  },
  state: {
    token: {},
    compInfo: {},
    // tenantID: '000',
    // userID: '0000',
    // viewMenus: [],
    // gettedNewMessage: null
  },
  mutations: {
      setToken(state, obj) {
        localStorage.token = JSON.stringify(obj)
        state.token = { ...obj }
      },
      setCompanyInfo(state, obj) {
        localStorage.compInfo = JSON.stringify(obj)
        state.compInfo = { ...obj }
      },
    //   setUserId(state, obj) {
    //     state.userID = obj.userID
    //     state.tenantID = obj.tenantID
    //   },
    //   getNewMessage(state, num) {
    //     state.gettedNewMessage = num
    //   }
    }
  })