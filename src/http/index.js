import './config';
import login from './login'
import home from './home'
let api = {
    ...login,
    ...home
};
export default api;
