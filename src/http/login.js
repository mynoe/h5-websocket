import axios from 'axios'

let login = {
    // login
    login(params){
        return axios.post('mobile/login',params)
    },
    // check-band
    checkBand(params){
        return axios.post('mobile/band-status',params)
    },
    // get-bind-company
    getBindCompany(params){
        return axios.post('mobile/get-invite',params)
    },
    // bind
    goBind(params){
        return axios.post('mobile/band',params)
    },
    // retreat bind
    retreatBind(params){
        return axios.post('mobile/band-over',params)
    },
    // 
    getQrCode(str){
        return axios.get('qr?str='+str,)
    }
}

export default login