import axios from 'axios';
import {Notify} from 'vant';
axios.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8';
axios.defaults.baseURL = process.env.REACT_APP_API_BASE_URL
axios.defaults.baseURL = '/api';
// if(process.env.NODE_ENV==='production'){
//   axios.defaults.baseURL = 'saas.haolaoban123.com/api/v1'
// }else{
//   axios.defaults.baseURL = '/api/v1'
// }

// console.log('process.env',process)
// console.log('base_url',axios.defaults.baseURL)
axios.defaults.withCredentials = true;

// let token = localStorage.getItem('token');

// 添加请求拦截器
axios.interceptors.request.use(
  config => {
    if (localStorage.getItem('token')) {
      // 如果token存在
      config.headers.Authorization = localStorage.getItem('token');
    }
    return config;
  },
  error => {
    // 对请求错误做些什么
    return Promise.reject(error);
  },
);

// 添加响应拦截器
axios.interceptors.response.use(
  response => {
    if(response.data){
      if(response.data.code === 2){
        localStorage.setItem('token','')
        // window.location.href='/login'
      }
    }
    return response;
  },
  error => {
    if (error['response']) {
      switch (error['response'].status) {
        case 505:
            Notify({message:'服务端错误!'})
            break;
        case 401:
            Notify({message:error.response.statusText})
        break;
        case 500:
            Notify({message:error.response.data.msg})
        break;
        default:
            Notify({message:error.response.data.msg})
      }
    }
    return Promise.reject(error);
  },
);
